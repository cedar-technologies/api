<?php

namespace App\Http\Controllers\Uwc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uwc\EmployeeShift;
use App\Models\Uwc\Employee;
use Carbon\Carbon;
use App\Http\Requests\Uwc\CreateEmployeeShiftRequest;

class EmployeeShiftApiController extends Controller
{
    public function create(CreateEmployeeShiftRequest $request)
    {
        $employee = Employee::where('card_no', $request->card_no)->first();
        
        if (!$employee) {
            return response()->json(['message' => 'Employee with the given card number not found.'], 404);
        }
        
        try {
            $pfr_no = Carbon::parse($request->clock_in_datetime)->format('ymd') . $employee->employee_id;
            
            EmployeeShift::create(
                [
                    'employee_id'        => $employee->employee_id,
                    'pfr_no'             => $pfr_no,
                    'clock_in_datetime'  => $request->clock_in_datetime,
                    'production_line_no' => $request->production_line_no,
                    'station_no'         => $request->station_no,
                ]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(
            [
                'message' => 'Employee shift record has been created.',
                'attributes' => [
                    'employee_id' => $employee->employee_id,
                    'pfr_no' => $pfr_no,
                    'name' => $employee->name
                ]
            ],
            200
        );
    }

    public function update(Request $request)
    {
        $employee_shift = EmployeeShift::where('employee_id', $request->employee_id)
            ->where('pfr_no', $request->pfr_no)
            ->where('production_line_no', $request->production_line_no)
            ->where('station_no', $request->station_no)
            ->whereNull('clock_out_datetime')
            ->first();
            
        if (!$employee_shift) {
            return response()->json(['message' => 'Clock in record of the given employee for the day not found, cannot update clock out.'], 404);
        }

        try {
            $start = Carbon::parse($employee_shift->clock_in_datetime);
            $end = Carbon::parse($request->clock_out_datetime);
            $working_hours = $end->diffInMinutes($start);

            $employee_shift->update(
                [
                    'clock_out_datetime' => $request->clock_out_datetime,
                    'working_hours'      => $working_hours
                ]
            );
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['message' => 'Employee shift record has been updated.'], 200);
    }
}
