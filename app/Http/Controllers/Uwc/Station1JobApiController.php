<?php

namespace App\Http\Controllers\Uwc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uwc\Station1Job;
use App\Models\Uwc\IncompleteBatch;
use App\Models\Uwc\Defect;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Uwc\CreateStation1JobRequest;

class Station1JobApiController extends Controller
{
    public function create(CreateStation1JobRequest $request)
    {
        try {
            DB::transaction(function() use ($request) {
                $batch_id = Station1Job::create(
                    [
                        'employee_id'             => $request->employee_id,
                        'production_line_no'      => $request->production_line_no,
                        'body_traceability_no'    => $request->body_traceability_no,
                        'body_subtraceability_no' => $request->body_subtraceability_no,
                        'silicon_traceability_no' => $request->silicon_traceability_no,
                        'pfr_1_no'                => $request->pfr_1_no,
                        'batch_start_datetime'    => $request->batch_start_datetime ?? null,
                        'batch_end_datetime'      => $request->batch_end_datetime ?? null,
                        'completed'               => $request->completed,
                        'incomplete'              => $request->incomplete_body,
                        'reason_of_incompletion'  => $request->reason_of_incompletion,
                    ]
                )->id;

                foreach ($request->defects as $defect) {
                    Defect::create(
                        [
                            'employee_id'        => $request->employee_id,
                            'production_line_no' => $request->production_line_no,
                            'station_no'         => 1,
                            'object_type'        => $defect['object_type'],
                            'traceability_no'    => $defect['traceability_no'],
                            'pfr_no'             => $request->pfr_1_no,
                            'units'              => $defect['units'],
                            'reason'             => $defect['reason'],
                            'batch_id'           => $batch_id
                        ]
                    );
                }
                
                // Create incomplete batch record if there's an incomplete batch
                if ($request->incomplete_body > 0) {
                    IncompleteBatch::create(
                        [
                            'traceability_no'        => $request->body_traceability_no,
                            'traceability_type'      => config('staticdata.traceability_type.0'),
                            'station_no'             => 1,
                            'incomplete'             => $request->incomplete_body,
                        ]
                    );
                }

                if ($request->incomplete_silicon > 0) {
                    IncompleteBatch::create(
                        [
                            'traceability_no'        => $request->silicon_traceability_no,
                            'traceability_type'      => config('staticdata.traceability_type.1'),
                            'station_no'             => 1,
                            'incomplete'             => $request->incomplete_silicon,
                        ]
                    );
                }

                // Update previous traceabilities within the same batch
                if ($request->batch_start_datetime) {
                    $other_traceabilities = Station1Job::where(
                        [
                            'pfr_1_no' => $request->pfr_1_no,
                            'batch_start_datetime' => NULL,
                            'batch_end_datetime' => NULL
                        ]
                    )->get();

                    foreach ($other_traceabilities as $traceability) {
                        $traceability->update(
                            [
                                'batch_start_datetime' =>  $request->batch_start_datetime,
                                'batch_end_datetime' =>  $request->batch_end_datetime
                            ]
                        );
                    }
                }
            });
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['message' => 'Station 1 job record has been created.'], 200);
    }
    
    public function getIncompleteUnits(Request $request)
    {
        try {
            $incomplete_batch = IncompleteBatch::select('id', 'incomplete')
                ->where(
                    [
                        'traceability_no' => $request->traceability_no,
                        'traceability_type' => $request->traceability_type,
                        'station_no' => 1
                    ]
                )->first();

            if ($incomplete_batch) {
                $incomplete_units = $incomplete_batch->incomplete;
                $previously_incomplete = true;
                $message = 'Get incomplete units success.';
                $incomplete_batch->delete();
            }

            return response()->json(
                [
                    'message' => $message ?? 'No incomplete units found for the batch.',
                    'attributes' => [
                        'previously_incomplete' => $previously_incomplete ?? false,
                        'units' => $incomplete_units ?? 1000,
                    ]
                ],
                200
            );

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
