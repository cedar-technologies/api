<?php

namespace App\Http\Controllers\Uwc;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Uwc\Station2Job;
use App\Models\Uwc\Station3Job;
use App\Models\Uwc\IncompleteBatch;
use App\Models\Uwc\Defect;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Uwc\CreateStation3JobRequest;

class Station3JobApiController extends Controller
{
    public function create(CreateStation3JobRequest $request)
    {
        try {
            DB::transaction(function() use ($request) {
                $batch_id = Station3Job::create(
                    [
                        'employee_id'                 => $request->employee_id,
                        'production_line_no'          => $request->production_line_no,
                        'body_traceability_no'        => $request->body_traceability_no,
                        'body_subtraceability_no'     => $request->body_subtraceability_no,
                        'pouch_traceability_no'       => $request->pouch_traceability_no,
                        'body_tube_traceability_no'   => $request->body_tube_traceability_no,
                        'pfr_1_no'                    => $request->pfr_1_no,
                        'pfr_2_no'                    => $request->pfr_2_no,
                        'pfr_3_no'                    => $request->pfr_3_no,
                        'batch_start_datetime'        => $request->batch_start_datetime ?? null,
                        'batch_end_datetime'          => $request->batch_end_datetime ?? null,
                        'completed'                   => $request->completed,
                        'incomplete'                  => $request->incomplete_body,
                        'reason_of_incompletion'      => $request->reason_of_incompletion,
                    ]
                )->id;
                
                foreach ($request->defects as $defect) {
                    Defect::create(
                        [
                            'employee_id'        => $request->employee_id,
                            'production_line_no' => $request->production_line_no,
                            'station_no'         => 3,
                            'object_type'        => $defect['object_type'],
                            'traceability_no'    => $defect['traceability_no'],
                            'pfr_no'             => $request->pfr_3_no,
                            'units'              => $defect['units'],
                            'reason'             => $defect['reason'],
                            'batch_id'           => $batch_id
                        ]
                    );
                }

                // Create incomplete batch record if there's an incomplete batch
                if ($request->incomplete_body > 0) {
                    IncompleteBatch::create(
                        [
                            'traceability_no'        => $request->body_traceability_no,
                            'traceability_type'      => config('staticdata.traceability_type.0'),
                            'station_no'             => 3,
                            'incomplete'             => $request->incomplete_body,
                        ]
                    );
                }

                if ($request->incomplete_pouch > 0) {
                    IncompleteBatch::create(
                        [
                            'traceability_no'        => $request->pouch_traceability_no,
                            'traceability_type'      => config('staticdata.traceability_type.3'),
                            'station_no'             => 3,
                            'incomplete'             => $request->incomplete_pouch,
                        ]
                    );
                }

                if ($request->incomplete_body_tube > 0) {
                    IncompleteBatch::create(
                        [
                            'traceability_no'        => $request->body_tube_traceability_no,
                            'traceability_type'      => config('staticdata.traceability_type.4'),
                            'station_no'             => 3,
                            'incomplete'             => $request->incomplete_body_tube,
                        ]
                    );
                }

                // Update previous traceabilities within the same batch
                if ($request->batch_start_datetime) {
                    $other_traceabilities = Station3Job::where(
                        [
                            'pfr_3_no' => $request->pfr_3_no,
                            'batch_start_datetime' => NULL,
                            'batch_end_datetime' => NULL
                        ]
                    )->get();

                    foreach ($other_traceabilities as $traceability) {
                        $traceability->update(
                            [
                                'batch_start_datetime' =>  $request->batch_start_datetime,
                                'batch_end_datetime' =>  $request->batch_end_datetime
                            ]
                        );
                    }
                }
            });
        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }

        return response()->json(['message' => 'Station 3 job record has been created.'], 200);
    }
    
    public function getIncompleteUnits(Request $request)
    {
        try {
            $incomplete_batch = IncompleteBatch::select('id', 'incomplete')
                ->where(
                    [
                        'traceability_no' => $request->traceability_no,
                        'traceability_type' => $request->traceability_type,
                        'station_no' => 3
                    ]
                )->first();

            if ($incomplete_batch) {
                $incomplete_units = $incomplete_batch->incomplete;
                $previously_incomplete = true;
                $message = 'Get incomplete units success.';
                $incomplete_batch->delete();
            } else if ($request->traceability_type == config('staticdata.traceability_type.0')) {
                $station2_completed_units_sum = Station2Job::where(
                    [
                        'body_traceability_no' => $request->traceability_no,
                        'body_subtraceability_no' => $request->subtraceability_no,
                        'pfr_2_no' => $request->pfr_no,
                    ]
                )->sum('completed');

                if ($station2_completed_units_sum == 0) {
                    $station2_completed_units_sum = 1000;
                }
                
                $previously_incomplete = false;
            } else {
                $incomplete_units = 1000;
                $previously_incomplete = false;
            }

            return response()->json(
                [
                    'message' => $message ?? 'No incomplete units found for the batch.',
                    'attributes' => [
                        'previously_incomplete' => $previously_incomplete,
                        'units' => $incomplete_units ?? $station2_completed_units_sum,
                    ]
                ],
                200
            );

        } catch (\Exception $e) {
            \Log::error($e);
            return response()->json(['message' => $e->getMessage()], 422);
        }
    }
}
