<?php

namespace App\Http\Middleware;

use Closure;

class AuthUwc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->bearerToken() == config('app.token.uwc')) {
            return $next($request);
        }

        return response()->json(
            [
                'message' => 'Unauthorized.'
            ]
        , 401);
    }
}
