<?php

namespace App\Http\Middleware;

use Closure;
use Etime\Flysystem\Plugin\AWS_S3 as AWS_S3_Plugin;
use League\Flysystem\AwsS3v3\AwsS3Adapter;
use League\Flysystem\Filesystem;
use Aws\S3\S3Client;

class CheckAppVersionUwc
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!$request->headers->has('Version')) {
            return response()->json(
                [
                    'message' => "No header 'Version' specified." 
                ]
            , 406);
        }

        if (config('staticdata.app_version.latest') != $request->header('Version', '')) {
            $filename = 'v' . str_replace('.', '-', config('staticdata.app_version.latest')) . '.apk';
            $client_data = [
                'region' => config('filesystems.disks.s3.region'),
                'version' => 'latest',
            ];

            if (env('APP_ENV') == 'local') {
                $client_data += [
                    'credentials' => [
                        'key'    => config('filesystems.disks.s3.key'),
                        'secret' => config('filesystems.disks.s3.secret')
                    ]
                ];
            }

            $client = new S3Client($client_data);
            $adapter = new AwsS3Adapter($client, config('filesystems.disks.s3.bucket'));
            $filesystem = new Filesystem($adapter);
            $filesystem->addPlugin(new AWS_S3_Plugin\PresignedUrl());           
            $url = $filesystem->getPresignedUrl($filename);

            return response()->json(
                [
                    'message' => "Please update the app to the latest version.",
                    'attributes' => [
                        'latest_version' => config('staticdata.app_version.latest'),
                        'url' => $url
                    ]
                ]
            , 406);
        }

        return $next($request);
    }
}
