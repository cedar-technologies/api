<?php

namespace App\Http\Requests\Uwc;

use Illuminate\Foundation\Http\FormRequest;

class CreateEmployeeShiftRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'card_no'                   => 'required',
            'production_line_no'        => 'required|integer|max:' . config('staticdata.production_line_limit'),
            'clock_in_datetime'         => 'required|date',
            'station_no'                => 'required',
        ];
    }

    public function messages()
    {
        return [
            'production_line_no.max' => "Production line number cannot exceed the limit. Limit: " . config('staticdata.production_line_limit') . " production line",
        ];
    }
}
