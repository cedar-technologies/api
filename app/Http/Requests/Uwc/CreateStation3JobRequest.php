<?php

namespace App\Http\Requests\Uwc;

use Illuminate\Foundation\Http\FormRequest;

class CreateStation3JobRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'employee_id'               => 'required',
            'production_line_no'        => 'required|integer|max:' . config('staticdata.production_line_limit'),
            'body_traceability_no'      => 'required',
            'body_subtraceability_no'   => 'required',
            'pouch_traceability_no'     => 'required',
            'body_tube_traceability_no' => 'required',
            'pfr_1_no'                  => 'required',
            'pfr_2_no'                  => 'required',
            'pfr_3_no'                  => 'required',
            'batch_start_datetime'      => 'nullable|date_format:Y-m-d H:i:s',
            'batch_end_datetime'        => 'nullable|date_format:Y-m-d H:i:s',
            'completed'                 => 'required|integer',
            'incomplete_body'           => 'nullable|integer',
            'reason_of_incompletion'    => 'nullable',
            'defects'                   => 'sometimes|array',
            'defects.*.object_type'     => 'required|in:' . implode(',', config('validation.defect_object_type')),
            'defects.*.units'           => 'nullable',
            'defects.*.traceability_no' => 'required',
            'defects.*.reason'          => 'required|in:' . implode(',', config('validation.defect_reason.station3')),
        ];
    }

    public function messages()
    {
        return [
            'production_line_no.max' => "Production line number cannot exceed the limit. Limit: " . config('staticdata.production_line_limit') . " production line",
            'defects.*.reason.object_type' => "Invalid object type. Possible values: " . implode(',', config('validation.defect_object_type')),
            'defects.*.reason.in' => "Invalid reason. Possible values: " . implode(', ', config('validation.defect_reason.station3'))
        ];
    }
}
