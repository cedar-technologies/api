<?php

namespace App\Models\Uwc;

use Illuminate\Database\Eloquent\Model;

class IncompleteBatch extends Model
{
    protected $connection = 'uwc';
    
    public $table = 'incomplete_batches';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'traceability_no',
        'traceability_type',
        'pfr_no',
        'station_no',
        'incomplete'
    ];
}
