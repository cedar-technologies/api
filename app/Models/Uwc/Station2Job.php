<?php

namespace App\Models\Uwc;

use Illuminate\Database\Eloquent\Model;

class Station2Job extends Model
{
    protected $connection = 'uwc';
    
    public $table = 'station_2_jobs';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'employee_id',
        'production_line_no',
        'body_traceability_no',
        'body_subtraceability_no',
        'cap_traceability_no',
        'pfr_1_no',
        'pfr_2_no',
        'batch_start_datetime',
        'batch_end_datetime',
        'completed',
        'incomplete',
        'reason_of_incompletion'
    ];
}
