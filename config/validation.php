<?php

return [
    'defect_reason' => [
        'station1' => [
            'Impurities/Black Dot',
            'Dirty',
            'Short Material',
            'Bubbles',
            'Thread Damaged',
            'Flow Mark',
            'Excessive Material',
            'Body Damaged',
            'Oil Mark/Oily',
            'Rubber Length Out',
            'Rubber Location Out',
            'QA/Sample',
            'Other',
        ],
        'station2' => [
            'Impurities/Black Dot',
            'Dirty',
            'Cap Damaged',
            'Body Damaged',
            'Oil Mark/Oily',
            'Overpress',
            'Press Incomplete',
            'QA/Sample',
            'Other',
        ],
        'station3' => [
            'Impurities/Black Dot',
            'Seal Incomplete',
            'Seal Damaged',
            'Pouch Printing Problem',
            'Lot Printing Problem',
            'QA/Sample',
            'Other',
        ]
    ],
    'defect_object_type' => [
        'Body' => 'Body',
        'Cap' => 'Cap',
        'Rubber' => 'Rubber',
        'Pouch' => 'Pouch',
        'Body Tube' => 'Body Tube',
        'Other' => 'Other',
    ]
];