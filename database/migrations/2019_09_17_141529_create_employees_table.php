<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uwc')->create('employees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id')->index();
            $table->string('name');
            $table->string('card_no', 20)->index();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uwc')->dropIfExists('employees');
    }
}
