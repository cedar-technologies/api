<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeShiftsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uwc')->create('employee_shifts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id')->index();
            $table->string('pfr_no', 50)->index();
            $table->datetime('clock_in_datetime')->index();
            $table->unsignedTinyInteger('production_line_no')->index();
            $table->unsignedTinyInteger('station_no')->index();
            $table->unsignedSmallInteger('working_hours')->default(0);
            $table->datetime('clock_out_datetime')->index()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uwc')->dropIfExists('employee_shifts');        
    }
}
