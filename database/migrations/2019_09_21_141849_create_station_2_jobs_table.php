<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStation2JobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uwc')->create('station_2_jobs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id')->index();
            $table->unsignedTinyInteger('production_line_no')->index();
            $table->string('body_traceability_no')->index();
            $table->string('cap_traceability_no')->index();
            $table->string('pfr_1_no')->index();
            $table->string('pfr_2_no')->index();
            $table->datetime('batch_start_datetime')->index()->nullable();
            $table->datetime('batch_end_datetime')->index()->nullable();
            $table->unsignedSmallinteger('completed')->unsigned()->default(0);
            $table->unsignedSmallInteger('incomplete')->default(0);
            $table->text('reason_of_incompletion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uwc')->dropIfExists('station_2_jobs');
    }
}
