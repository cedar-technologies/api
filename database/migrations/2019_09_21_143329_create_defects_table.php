<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uwc')->create('defects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('employee_id')->index();
            $table->unsignedTinyInteger('production_line_no')->index();
            $table->unsignedTinyInteger('station_no')->nullable();
            $table->datetime('report_datetime')->nullable();
            $table->string('object_type')->nullable();
            $table->string('traceability_no')->nullable();
            $table->string('pfr_no')->nullable();
            $table->text('reason');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uwc')->dropIfExists('defects');
    }
}
