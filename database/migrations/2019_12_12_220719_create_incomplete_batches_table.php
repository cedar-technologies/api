<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIncompleteBatchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uwc')->create('incomplete_batches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('traceability_no', 100)->index();
            $table->string('traceability_type', 100)->index();
            $table->unsignedTinyInteger('station_no')->index();
            $table->unsignedSmallInteger('incomplete')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uwc')->dropIfExists('incomplete_batches');
    }
}
