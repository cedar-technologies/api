<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBatchIdAndUnitsToDefectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('uwc')->table('defects', function (Blueprint $table) {
            $table->unsignedBigInteger('batch_id')->nullable();
            $table->unsignedSmallInteger('units')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('uwc')->table('defects', function (Blueprint $table) {
            $table->dropColumn('batch_id');
            $table->dropColumn('units');
        });
    }
}
