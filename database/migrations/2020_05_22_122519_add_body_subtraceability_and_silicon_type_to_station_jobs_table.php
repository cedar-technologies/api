<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBodySubtraceabilityAndSiliconTypeToStationJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('station_1_jobs', function (Blueprint $table) {
            $table->string('body_subtraceability_no')->index();
            $table->string('silicon_traceability_no')->index();
        });

        Schema::table('station_2_jobs', function (Blueprint $table) {
            $table->string('body_subtraceability_no')->index();
        });

        Schema::table('station_3_jobs', function (Blueprint $table) {
            $table->string('body_subtraceability_no')->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('station_1_jobs', function (Blueprint $table) {
            $table->dropColumn('body_subtraceability_no');
            $table->dropColumn('silicon_traceability_no');
            $table->dropColumn('incomplete_silicon');
        });

        Schema::table('station_2_jobs', function (Blueprint $table) {
            $table->dropColumn('body_subtraceability_no');
        });

        Schema::table('station_3_jobs', function (Blueprint $table) {
            $table->dropColumn('body_subtraceability_no');
        });
    }
}
