<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Uwc','prefix' => 'uwc', 'as' => 'uwc.', 'middleware' => ['auth.uwc', 'version.uwc']], function () {
    Route::group(['prefix' => 'employee-shifts', 'as' => 'employee-shifts.'], function () {
        Route::post('/', 'EmployeeShiftApiController@create')->name('create');
        Route::post('/update', 'EmployeeShiftApiController@update')->name('update');
    });
    Route::group(['prefix' => 'station-jobs', 'as' => 'station-jobs.'], function () {
        Route::get('/incomplete-units/1', 'Station1JobApiController@getIncompleteUnits')->name('1.incomplete-units');
        Route::get('/incomplete-units/2', 'Station2JobApiController@getIncompleteUnits')->name('2.incomplete-units');
        Route::get('/incomplete-units/3', 'Station3JobApiController@getIncompleteUnits')->name('3.incomplete-units');
        Route::post('/1', 'Station1JobApiController@create')->name('1.create');
        Route::post('/2', 'Station2JobApiController@create')->name('2.create');
        Route::post('/3', 'Station3JobApiController@create')->name('3.create');
    });
    Route::group(['prefix' => 'defects', 'as' => 'defects.'], function () {
        Route::post('/', 'DefectApiController@create')->name('create');
    });
});
